import './App.css';
import Mode from './components/Mode';
import {createTheme, ThemeProvider} from '@mui/material/styles'
import { Paper, Typography} from '@mui/material';
import { useState } from 'react';

function App() {

  const [darkMode, setDarkMode]= useState(true);
  const theme = createTheme({
    palette: {
      mode: darkMode ? "dark" : "light" ,
    }
  });
  return (
    <ThemeProvider theme={theme}>
      <Paper style={{height:"200vh"}} align="center">
      <Mode check={darkMode} change={() => setDarkMode(!darkMode)}/> 
      <Typography
      color="red"
      >
        The App is Under Development, Comming Soon!!
      </Typography>
    </Paper>
    </ThemeProvider>
  );
}

export default App;
