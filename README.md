# airport

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/darkarchlinux/airport.git
git branch -M main
git push -uf origin main
```

<!-- coding with honor back to top link: See: https://gitlab.com/darkarchlinux/airport.git -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** Thanks again! Now go create something Awesome! :D
-->



<!-- PROJECT AIRPORT -->
<!--
*** this app is to make traveling easy especially at the airport.
*** you can use your phone, it will show where and when you need to be.
*** it in the future, i will add the stores and places for food and entertainment.
-->

<!-- PROJECT  AIRPORT -->
<br />
<div align="center">
  <a href="https://gitlab.com/darkarchlinux/airport.git">
  </a>
  <h3 align="center">AIRPORT</h3>

  <p align="center">
    Eas into learning about airports!
    <br />
    <a href="https://gitlab.com/darkarchlinux/airport.git"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

everytime I go to airport, I always get confused and having a hard time tring to find where everything is. The crowed, the traffic and people rushing or slowing down it makes using airport not fun and inconvenient. Thus, I came up with the idea of what would be the easiest way to go about your flight without stressing out how to get there. 

Here's why:
* People will spend time on enjoying thier flight rather than focusing on how to finding everything.
* Perhaps you get to enjoy a meal or a drink, or chatting up a stranger instead of working about your flight.
* this app is gonna be on your phone, and you gonna be on your phone listening to your favorite songs, mine as well checking this app out is better than trying to find someone help you find things, cause people at the airport do not want to talk.


<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

Below are tools that are used to build this app

* [![React][React.js]][React-url]
* [![Java][Java.java]][Java-url]
* [![Springboot][Springboot]][Springboot-url]
* [![Database][Mysql-Mariadb]][Mysql-url]




<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started
Below is a simple direction fo how to set up the environment.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* BackEnd
  ```sh
  run run the app from intellij btn
  ```
* FrontEnd
  ```sh
  npm install -g yarn
  yarn create react-app <app name>
  yarn start
  ```

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._


1. Clone the repo
   ```sh
   git clone https://github.com/hewrtarkhany/airport
   ```
2. Install yarn packages
   ```sh
   yarn install
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage
Using this app to navigate in the Airport because in the future, it gets crowed in at the airport you will need some guidance. 
this app is built to serve that purpose. Enjoy itm share some positive thoughts.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [x] Add Changelog
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
    - [ ] Chinese
    - [ ] Spanish

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

Contributions is what makes a project to be a team project rather than one person. In the open source style, we work hard to serve humanity, we fix future problems.
we create what we think people needs for free, we find joy in simple things. 

*** COMMIT MORE PUSH LESS
1. Create your Feature Branch (`git checkout -b feature-number-action`)
2. Commit your Changes (`git commit -m 'add what you did'`)
3. Push to the Branch (`git push origin feature-number-action`)
4. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License
there is not a license for this, it's an open source enviorment.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Your Name - Dark Arch - darkarchlinux@gmail.com

Project Link: https://gitlab.com/darkarchlinux/airport.git

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS-->

[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/

[Java.java]: https://img.shields.io/badge/java-backend-yellow
[Java-url]: https://java.com

[Springboot]: https://img.shields.io/badge/Spring-Boot-blue
[Springboot-url]: https://spring.io

[Mysql-Mariadb]: https://img.shields.io/badge/mysql-mariadb-orange
[Mysql-url]: https://www.mysql.com/

